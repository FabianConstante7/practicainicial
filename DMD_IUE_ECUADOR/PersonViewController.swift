//
//  PersonViewController.swift
//  DMD_IUE_ECUADOR
//
//  Created by formando on 25/10/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit

class PersonViewController: UIViewController {
    
    var nuevaPersona = Persona()
    
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var nombre: UITextField!
    
    
    @IBOutlet weak var apellido: UITextField!
    
    @IBOutlet weak var nacionalidad: UITextField!
    
    
    @IBAction func ButtonPressed(_ sender: UIButton) {
    nuevaPersona.firstName=nombre.text
    nuevaPersona.lastName=apellido.text
    nuevaPersona.nationality=nacionalidad.text!
        
    textLabel.text=nuevaPersona.fullName()
    
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
