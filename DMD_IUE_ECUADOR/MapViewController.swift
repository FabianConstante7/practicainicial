//
//  MapViewController.swift
//  DMD_IUE_ECUADOR
//
//  Created by formando on 26/10/2016.
//  Copyright © 2016 ipleiria. All rights reserved.
//  SWIFT INTERFAZ => PROTOCOLO

import UIKit
import CoreLocation
import MapKit


class MapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate { // PROTOCOLO DE LOCALIZACION Y DELEGADO DE MAPA
    
    
    @IBOutlet weak var latLabel: UILabel!
    
    @IBOutlet weak var lngLabel: UILabel!
    
    @IBOutlet weak var map: MKMapView!
    
    //GESTOR LOCALIZACION
    let locationManager = CLLocationManager()
    //VALOR DE LOCALIZACION
    var location:CLLocationCoordinate2D?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // CONFIGURACIONES PARA LA VISTA
        locationManager.delegate = self //DELEGACION DE LA PROPIA CLASE PARA EL MANEJO DE LOCALIZACION
        locationManager.desiredAccuracy = kCLLocationAccuracyBest //MAMERA DE TOMAR LOS DATOS YA SEA GPS, WIFI AP LOCATION, GSM LOCATION
        locationManager.distanceFilter = kCLDistanceFilterNone // FILTRO DE DISTANCIA -> VARIACION DE LOCALIZACION MINIMA
        //PEDIDO DE LOCALIZACION
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        //CONFIGURACION DE MAPA
        map.delegate = self
        //CENTRO DE MAPA
        map.showsUserLocation = true
    }
    
    //IMPLEMENTAR EL DELEGADO
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("ERROR DE LOCALIZACION")
    }
    
    //PARA PEDIR AUTORIZACION DE LA LOCALIZACION
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse { //VALIDAR SI YA POSEEMOS LOCALIZACION
            locationManager.startUpdatingLocation()
        }
    }
    
    //CUANDO MUDAMOS LA LOCALIZACION
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationObj = locations.last
        let coord = locationObj?.coordinate
        if let c = coord {
            latLabel.text = "\(c.latitude)"
            lngLabel.text = "\(c.longitude)"
            print("LAT: \(c.latitude) LNG: \(c.longitude)")
            //ACTUALIZAR LA VARIABLE DE MAPA
            location = CLLocationCoordinate2D(latitude: c.latitude, longitude: c.longitude)
        }
    }
    
    //CUANDO LA VISTA VUELVE A CARGAR
    override func viewDidAppear(_ animated: Bool) {
        if let loc = location{
            let span = MKCoordinateSpanMake(0.1, 0.1) //ZOOM DE MAPA
            let region = MKCoordinateRegionMake(loc, span) //CREAR UNA REGION
            map.setRegion(region, animated: true) // CAMBIAR LA REGION DEL MAPA
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    

}
