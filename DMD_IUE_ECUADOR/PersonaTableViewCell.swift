//
//  PersonaTableViewCell.swift
//  DMD_IUE_ECUADOR
//
//  Created by formando on 26/10/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit

class PersonaTableViewCell: UITableViewCell {
    
    
   
    
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    
    //variable del tipo persona
    var person:Persona!{
    // Antes de que la persona  tenga el contenido de del seet will antes , set despues
        didSet{
        firstNameLabel.text = self.person.firstName
        lastNameLabel.text=self.person.lastName
        
        }
    
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
